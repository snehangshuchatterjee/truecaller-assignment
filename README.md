# The Truecaller Blog

## About the App

This is an Application to display the blogs of Truecaller, which are being fetched from a wordpress API. The tech stack used in the application are as follows:
- Javascript (ES2015+)
- ReactJS
- Redux
- React Router
- Axios
- Redux-Axios-Middleware
- React-Paginate
- Sass
- ESLint(Airbnb config)
- Prettier, etc

## Starting the Application
In order to start the application, the following steps should be performed:
- Clone the repository from [this link](https://gitlab.com/snehangshuchatterjee/truecaller-assignment)
- Run the `yarn` or `yarn install` command. This command will install all the dependencies required for the application to work
- Run `yarn start`. This will run the application on your localhost and it will be loaded on a browser.

### Additional Steps
ESLint has also been configured in the application following the airbnb standards. if you want to test the application for linting errors, you can run `yarn run lint` script from the root folder.

## Scope for Improvement

There is always a scope for improvement in everything, as nothing is perfect. So if we want to talk about the scope of improvement for this application, the following things take a priority

- `Adding Unit Tests` I have worked on tools like Jest, enzyme etc, but couldn't implement it due to lack of time.
- `More Responsive Layout` Although I have made the application responsive, and tested it on multiple devices, it could have been better by using some libraries like Bootstrap or Material UI. However I didn't use them as I assumed that I shouldn't use them to highlight my CSS skills

## Screenshots from the Application

- List view in Desktop:

![List view in Desktops](./src/assets/List_Desktop.png)
- List view in Mobiles:

![List view in Mobiles](./src/assets/List_Mobile.png)
- Detail view in Desktop:

![Detail view in Desktop](./src/assets/Details_Desktop.png)
- Detail view in Mobile:

![Detail view in Mobile](./src/assets/Details_Mobile.png)

The images have at some places been reduced in the font size to accommodate more items in them

## Testing Done

I have done a limited, time-based testing of the application checking the features. Also, I have tested it for responsiveness in Multiple Devices:
- Apple Macbook Pro 2013 (Small Screen Laptop, 13")
- Apple Macbook Pro 2019 (Big screen laptop, 16")
- Samsung Galaxy S20 (Chrome Browser, Android)
- Apple iPhone 11 (Chrome Browser, iOS)