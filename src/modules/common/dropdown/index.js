/* eslint-disable jsx-a11y/no-onchange */
import React from "react"
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from "prop-types"

const DropdownComponent = (props) => {
  const {
    category,
    cssClass,
    defaultOption,
    dropdownName,
    onChange,
    optionsArray,
  } = props

  const selectedCategory = category === "" ? "all categories" : category

  return (
    <>
      <select
        className={cssClass}
        name={dropdownName}
        id={dropdownName}
        onChange={onChange}
        value={selectedCategory}
      >
        {defaultOption && (
          <option value={defaultOption.toLowerCase()}>{defaultOption}</option>
        )}
        {optionsArray &&
          optionsArray.map((option) => (
            <option key={option.ID} value={option.slug}>
              {option.name}
            </option>
          ))}
      </select>
    </>
  )
}

DropdownComponent.propTypes = {
  category: PropTypes.string.isRequired,
  cssClass: PropTypes.string.isRequired,
  defaultOption: PropTypes.string.isRequired,
  dropdownName: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  optionsArray: PropTypes.arrayOf(
    PropTypes.shape({
      ID: PropTypes.number,
      name: PropTypes.string,
      slug: PropTypes.string,
      description: PropTypes.string,
      post_count: PropTypes.number,
      feed_url: PropTypes.string,
      parent: PropTypes.number,
      meta: PropTypes.shape(),
    })
  ).isRequired,
}

export default DropdownComponent
