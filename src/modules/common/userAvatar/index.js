import React from "react"
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from "prop-types"

import Image from "../image"
import "./userAvatar.scss"

const getDateInCorrectFormat = (date) => {
  let dateString = ""
  const inputDate = new Date(date)
  const options = { month: "short" }
  const monthName = new Intl.DateTimeFormat("en-US", options).format(inputDate)

  dateString = `${monthName} ${inputDate.getDate()}, ${inputDate.getFullYear()}`

  return dateString
}

const UserAvatar = (props) => {
  const { author, date } = props

  if (author) {
    return (
      <div className="user-details">
        <div className="user-avatar">
          <Image
            source={author.avatar_URL}
            alt="Header Image"
            width="100%"
            height="auto"
            cssClass="header-image"
          />
        </div>
        <div className="right-content">
          <div className="user-name">{author.name}</div>
          <div className="publish-date">{getDateInCorrectFormat(date)}</div>
        </div>
      </div>
    )
  }
  return ""
}

UserAvatar.propTypes = {
  author: PropTypes.shape({
    ID: PropTypes.number.isRequired,
    login: PropTypes.string,
    email: PropTypes.bool,
    name: PropTypes.string,
    first_name: PropTypes.string,
    last_name: PropTypes.string,
    nice_name: PropTypes.string,
    URL: PropTypes.string,
    avatar_URL: PropTypes.string,
    profile_URL: PropTypes.string,
  }).isRequired,
  date: PropTypes.string.isRequired,
}

export default UserAvatar
