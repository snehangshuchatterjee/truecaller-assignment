import React from "react"
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from "prop-types"

import "./image.scss"

const Image = (props) => {
  const { source, alt, width, height, cssClass } = props

  return (
    <div className="image-base">
      <img
        src={source}
        alt={alt}
        width={width}
        height={height}
        className={cssClass}
      />
    </div>
  )
}

Image.propTypes = {
  source: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  width: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
  cssClass: PropTypes.string.isRequired,
}

export default Image
