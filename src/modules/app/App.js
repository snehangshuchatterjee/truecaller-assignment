import React from "react"
import { Provider } from "react-redux"
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

import Routes from "../../router"
import store from "../../redux/store"
import "./app.scss"
import ErrorBoundary from "../errorBoundary"

function App() {
  return (
    <Provider store={store}>
      <div className="app">
        <ErrorBoundary>
          <Routes />
        </ErrorBoundary>
      </div>
    </Provider>
  )
}

export default App
