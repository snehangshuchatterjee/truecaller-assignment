import { React } from "react"
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from "prop-types"

import Logo from "./logo"
import HeaderImage from "../../assets/header.jpg"
import "./header.scss"
import Image from "../common/image"

const Header = (props) => {
  const { headerImageSource } = props

  return (
    <div className="header">
      <div className="logo">
        <Logo width="100px" />
      </div>

      <div className="header-image-base">
        <Image
          source={headerImageSource || HeaderImage}
          alt="Header Image"
          width="100%"
          height="auto"
          cssClass="header-image"
        />
        {!headerImageSource && (
          <div className="header-text">The Truecaller Blog</div>
        )}
      </div>
    </div>
  )
}

Header.propTypes = {
  headerImageSource: PropTypes.string,
}

Header.defaultProps = {
  headerImageSource: undefined,
}

export default Header
