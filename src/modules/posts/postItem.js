/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable camelcase */
import React from "react"
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from "prop-types"

import Image from "../common/image"
import "./posts.scss"

import NoImage from "../../assets/noImage.jpeg"
import { postProp } from "../../propTypes"

const getCategoryName = (categoryData) => {
  const categoryObjKeys = Object.keys(categoryData)

  return categoryObjKeys.join(", ")
}

const shortenLongText = (text) => {
  const result = text.length > 50 ? text.slice(0, 40).concat("...") : text

  return result
}

const getTimeDifference = (postTime) => {
  const currentTime = new Date().getTime()
  const postedTime = new Date(postTime).getTime()
  const timeDifferenceInSeconds = Math.floor(currentTime - postedTime) / 1000
  const timeDifferenceInMins =
    timeDifferenceInSeconds > 59
      ? Math.floor(timeDifferenceInSeconds / 60)
      : undefined
  const timeDifferenceInHours =
    timeDifferenceInMins > 59
      ? Math.floor(timeDifferenceInMins / 60)
      : undefined
  const timeDifferenceInDays =
    timeDifferenceInHours > 23
      ? Math.floor(timeDifferenceInHours / 24)
      : undefined
  const timeDifferenceInMonths =
    timeDifferenceInDays > 30
      ? Math.floor(timeDifferenceInDays / 30)
      : undefined
  const timeDifferenceInYears =
    timeDifferenceInMonths > 11
      ? Math.floor(timeDifferenceInMonths / 12)
      : undefined
  let result = ""

  if (timeDifferenceInYears) {
    result = `${timeDifferenceInYears} ${
      timeDifferenceInYears > 1 ? "years" : "year"
    } ago`
  } else if (timeDifferenceInMonths) {
    result = `${timeDifferenceInMonths} ${
      timeDifferenceInMonths > 1 ? "months" : "month"
    } ago`
  } else if (timeDifferenceInDays) {
    result = `${timeDifferenceInDays} ${
      timeDifferenceInDays > 1 ? "days" : "day"
    } ago`
  } else if (timeDifferenceInHours) {
    result = `${timeDifferenceInHours} ${
      timeDifferenceInHours > 1 ? "hours" : "hour"
    } ago`
  } else if (timeDifferenceInMins) {
    result = `${timeDifferenceInMins} ${
      timeDifferenceInMins > 1 ? "mins" : "min"
    } ago`
  } else if (timeDifferenceInSeconds) {
    result = `${timeDifferenceInSeconds} ${
      timeDifferenceInSeconds > 1 ? "seconds" : "second"
    } ago`
  }

  return result
}

const PostItem = (props) => {
  const { post, onClick } = props
  const { date, title, post_thumbnail, categories } = post

  const imageSource = post_thumbnail ? post_thumbnail.URL : NoImage

  return (
    <div className="post">
      <div className="post-item" onClick={() => onClick(post)}>
        <div className="post-item-header">{getCategoryName(categories)}</div>
        <div className="header-image-base">
          <Image
            source={imageSource}
            alt="Header Image"
            width="100%"
            height="auto"
            cssClass="header-image"
          />
        </div>
        <div className="post-item-content">
          {shortenLongText(title)}
          <div className="post-time">{getTimeDifference(date)}</div>
        </div>
      </div>
    </div>
  )
}

PostItem.propTypes = {
  post: postProp.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default PostItem
