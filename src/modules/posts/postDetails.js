/* eslint-disable react/no-danger */
import React from "react"
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from "prop-types"
import { connect } from "react-redux"
import Loader from "react-loader-spinner"

import "./posts.scss"
import Header from "../header"
import Footer from "../footer"
import UserAvatar from "../common/userAvatar"
import { postProp } from "../../propTypes"

const mapStateToProps = (state) => ({
  loading: state.homeReducer.loading,
  selectedPostDetails: state.homeReducer.selectedPostDetails,
})

const PostDetails = (props) => {
  const { loading, selectedPostDetails } = props

  return (
    <div className="post">
      {loading && (
        <div className="loader">
          <Loader
            type="ThreeDots"
            color="#00BFFF"
            height={100}
            width={100}
            timeout={5000000}
          />
        </div>
      )}
      {!loading && (
        <>
          <Header headerImageSource={selectedPostDetails.featured_image} />
          <div className="post-details">
            <div className="post-details-title">
              {selectedPostDetails.title}
            </div>
            <UserAvatar
              author={selectedPostDetails.author}
              date={selectedPostDetails.date}
            />
            <div
              className="post-details-content"
              dangerouslySetInnerHTML={{ __html: selectedPostDetails.content }}
            />
            {Object.keys(selectedPostDetails).length === 0 && (
              <div className="error-message">
                Something Went Wrong, Please go back to the previous
                screen.....!!!
              </div>
            )}
          </div>
          <Footer />
        </>
      )}
    </div>
  )
}

PostDetails.propTypes = {
  loading: PropTypes.bool.isRequired,
  selectedPostDetails: postProp.isRequired,
}

export default connect(mapStateToProps)(PostDetails)
