import React from "react"
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from "prop-types"
import DropdownComponent from "../common/dropdown"
import PostItem from "./postItem"
import { categoryProp, postProp } from "../../propTypes"

const PostList = (props) => {
  const { category, categories, posts, getSelectedCategory, onClick } = props

  return (
    <div className="post">
      <div className="content">
        <div className="blog">
          <div className="heading">Latest articles</div>
        </div>
        <DropdownComponent
          category={category}
          cssClass="category-dropdown"
          defaultOption="All Categories"
          dropdownName="Categories"
          optionsArray={categories}
          onChange={getSelectedCategory}
        />
        <div className="posts">
          {posts.length > 0 &&
            posts.map((post) => (
              <PostItem key={Math.random()} post={post} onClick={onClick} />
            ))}
        </div>
      </div>
    </div>
  )
}

PostList.propTypes = {
  category: categoryProp.isRequired,
  categories: PropTypes.arrayOf(categoryProp).isRequired,
  posts: PropTypes.arrayOf(postProp).isRequired,
  getSelectedCategory: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default PostList
