/* eslint-disable no-shadow */
import React, { useEffect, useState } from "react"
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { useHistory } from "react-router-dom"
import Loader from "react-loader-spinner"
import ReactPaginate from "react-paginate"

import Header from "../header"
import PostList from "../posts/postList"
import {
  clearPosts,
  fetchCategories,
  fetchPosts,
  fetchPostDetails,
} from "./home.actions"
import "./home.scss"
import Footer from "../footer"
import { categoryProp, postProp } from "../../propTypes"

const ITEMS_PER_PAGE = 20

const mapStateToProps = (state) => ({
  categories: state.homeReducer.categories,
  error: state.homeReducer.error,
  loading: state.homeReducer.loading,
  posts: state.homeReducer.posts,
  countOfPosts: state.homeReducer.countOfPosts,
})

const mapDispatchToProps = (dispatch) => ({
  fetchPosts: (category, pageNumber) =>
    dispatch(fetchPosts(category, pageNumber)),
  fetchCategories: () => dispatch(fetchCategories()),
  fetchPostDetails: (postSlug) => dispatch(fetchPostDetails(postSlug)),
  clearPosts: () => dispatch(clearPosts()),
})

const Home = (props) => {
  const {
    categories,
    clearPosts,
    fetchPosts,
    fetchCategories,
    countOfPosts,
    loading,
    posts,
    fetchPostDetails,
  } = props
  const [pageNumber, setPageNumber] = useState(0)
  const [category, setCategory] = useState("")
  const history = useHistory()

  useEffect(() => {
    fetchPosts(category, pageNumber + 1)
  }, [pageNumber])

  useEffect(() => {
    fetchCategories()
  }, [])

  useEffect(() => {
    clearPosts()
    fetchPosts(category, pageNumber + 1)
  }, [category])

  const getSelectedCategory = (event) => {
    const currentCategory = event.currentTarget.value
    if (currentCategory === "all categories") {
      setCategory("")
    } else {
      setCategory(currentCategory)
    }

    setPageNumber(0)
  }

  const getTotalNumberOfPages = () => Math.ceil(countOfPosts / ITEMS_PER_PAGE)

  const handlePageChange = (selectedPageNumber) => {
    setPageNumber(selectedPageNumber.selected)
  }

  const handlePostSelection = (post) => {
    fetchPostDetails(post.slug)
    history.push("/details")
  }

  return (
    <div className="home">
      {loading && (
        <div className="loader">
          <Loader
            type="ThreeDots"
            color="#00BFFF"
            height={100}
            width={100}
            timeout={5000}
          />
        </div>
      )}
      {!loading && (
        <>
          <Header />
          {posts.length > 0 && (
            <PostList
              categories={categories}
              category={category}
              posts={posts}
              getSelectedCategory={getSelectedCategory}
              onClick={handlePostSelection}
            />
          )}
          <div className="react-paginate">
            <ReactPaginate
              pageCount={getTotalNumberOfPages()}
              pageRangeDisplayed={3}
              marginPagesDisplayed={2}
              previousLabel="<"
              nextLabel=">"
              onPageChange={handlePageChange}
              forcePage={pageNumber}
              activeClassName="react-paginate-selected"
              disabledClassName="react-paginate-disabled"
            />
          </div>
          <Footer />
        </>
      )}
    </div>
  )
}

Home.propTypes = {
  categories: PropTypes.arrayOf(categoryProp).isRequired,
  clearPosts: PropTypes.func.isRequired,
  fetchPosts: PropTypes.func.isRequired,
  fetchCategories: PropTypes.func.isRequired,
  countOfPosts: PropTypes.number.isRequired,
  loading: PropTypes.bool.isRequired,
  posts: PropTypes.arrayOf(postProp).isRequired,
  fetchPostDetails: PropTypes.string.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
