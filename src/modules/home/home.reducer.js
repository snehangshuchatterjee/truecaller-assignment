import {
  FETCH_CATEGORIES,
  FETCH_CATEGORIES_FAIL,
  FETCH_CATEGORIES_SUCCESS,
  FETCH_POSTS,
  FETCH_POSTS_FAIL,
  FETCH_POSTS_SUCCESS,
  CLEAR_POSTS,
  FETCH_POST_DETAILS,
  FETCH_POST_DETAILS_FAIL,
  FETCH_POST_DETAILS_SUCCESS,
} from "./home.actions"

const initialState = {
  categories: [],
  error: false,
  loading: false,
  posts: [],
  selectedPostDetails: {},
  countOfPosts: 0,
}

export default function homeReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_CATEGORIES: {
      return {
        ...state,
        loading: true,
        error: false,
      }
    }
    case FETCH_CATEGORIES_FAIL: {
      return {
        ...state,
        loading: false,
        error: true,
      }
    }
    case FETCH_CATEGORIES_SUCCESS: {
      return {
        ...state,
        loading: false,
        categories: action.payload.data.categories,
      }
    }
    case FETCH_POSTS: {
      return {
        ...state,
        loading: true,
        error: false,
      }
    }
    case FETCH_POSTS_FAIL: {
      return {
        ...state,
        loading: false,
        error: true,
      }
    }
    case FETCH_POSTS_SUCCESS: {
      return {
        ...state,
        loading: false,
        posts: action.payload.data.posts,
        countOfPosts: action.payload.data.found,
      }
    }
    case FETCH_POST_DETAILS: {
      return {
        ...state,
        loading: true,
        error: false,
      }
    }
    case FETCH_POST_DETAILS_FAIL: {
      return {
        ...state,
        loading: false,
        error: true,
      }
    }
    case FETCH_POST_DETAILS_SUCCESS: {
      return {
        ...state,
        loading: false,
        selectedPostDetails: action.payload.data,
      }
    }
    case CLEAR_POSTS: {
      return {
        ...state,
        posts: [],
        countOfPosts: 0,
      }
    }
    default: {
      return initialState
    }
  }
}
