export const FETCH_POSTS = "FETCH_POSTS"
export const FETCH_POSTS_SUCCESS = "FETCH_POSTS_SUCCESS"
export const FETCH_POSTS_FAIL = "FETCH_POSTS_FAIL"
export const FETCH_CATEGORIES = "FETCH_CATEGORIES"
export const FETCH_CATEGORIES_SUCCESS = "FETCH_CATEGORIES_SUCCESS"
export const FETCH_CATEGORIES_FAIL = "FETCH_CATEGORIES_FAIL"
export const FETCH_POST_DETAILS = "FETCH_POST_DETAILS"
export const FETCH_POST_DETAILS_SUCCESS = "FETCH_POST_DETAILS_SUCCESS"
export const FETCH_POST_DETAILS_FAIL = "FETCH_POST_DETAILS_FAIL"
export const CLEAR_POSTS = "CLEAR_POSTS"

export const fetchPosts = (category, pageNumber) => {
  let apiURL

  if (category === "") {
    apiURL = `/posts/?fields=slu g,categories,slug,post_thumbnail,title,date&number=20&page=${pageNumber}`
  } else {
    apiURL = `/posts/?fields=slu g,categories,slug,post_thumbnail,title,date&number=20&page=${pageNumber}&category=${category}`
  }

  return {
    type: FETCH_POSTS,
    payload: {
      request: {
        url: apiURL,
      },
    },
  }
}

export const fetchPostDetails = (postID) => ({
  type: FETCH_POST_DETAILS,
  payload: {
    request: {
      url: `/posts/slug:${postID}?fields=featured_image,title,author,content,date`,
    },
  },
})

export const fetchCategories = () => ({
  type: FETCH_CATEGORIES,
  payload: {
    request: {
      url: `/categories`,
    },
  },
})

export const clearPosts = () => ({
  type: CLEAR_POSTS,
})
