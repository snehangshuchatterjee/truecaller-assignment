import React from "react"

import "./footer.scss"

const Footer = () => (
  <div className="footer">© True Software Scandinavia AB</div>
)

export default Footer
