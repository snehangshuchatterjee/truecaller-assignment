/* eslint-disable import/no-extraneous-dependencies */
import React from "react"
import { Router, Switch, Route } from "react-router-dom"
import { createBrowserHistory } from "history"

import Home from "../modules/home"
import PostDetails from "../modules/posts/postDetails"

const history = createBrowserHistory()

function Routes() {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/details" exact component={PostDetails} />
      </Switch>
    </Router>
  )
}

export default Routes
