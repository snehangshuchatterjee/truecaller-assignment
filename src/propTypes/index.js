// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from "prop-types"

export const categoryProp = PropTypes.shape({
  ID: PropTypes.number,
  name: PropTypes.string,
  slug: PropTypes.string,
  description: PropTypes.string,
  post_count: PropTypes.number,
  feed_url: PropTypes.string,
  parent: PropTypes.number,
  meta: PropTypes.shape(),
})

export const postProp = PropTypes.shape({
  date: PropTypes.string,
  title: PropTypes.string,
  slug: PropTypes.string,
  post_thumbnail: PropTypes.shape({
    ID: PropTypes.number,
    URL: PropTypes.string,
    guid: PropTypes.string,
    mime_type: PropTypes.string,
    width: PropTypes.number,
    height: PropTypes.number,
  }),
  categories: PropTypes.shape(),
})
