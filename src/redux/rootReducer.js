import { combineReducers } from "redux"

import homeReducer from "../modules/home/home.reducer"

const RootReducer = combineReducers({
  homeReducer,
})

export default RootReducer
